//
//  BISFilterOperation.h
//  BISFlickr2.0
//
//  Created by iliya on 27.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BISFlickr.h"

@interface BISFilterOperation : NSOperation

@property(nonatomic, strong) BISFlickr *flickr;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, copy) void(^returnFilteredImage)(BISFlickr*, UIImage*);

- (instancetype)initWithFlickr:(BISFlickr*)flickr imageToFilter:(UIImage*)image returnFilteredImage: (void(^)(BISFlickr*, UIImage*))returnFilteredImage;

@end
