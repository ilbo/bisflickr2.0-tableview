//
//  BISNavigationTable.m
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISNavigationTable.h"
#import "BISTableViewController.h"

@interface BISNavigationTable ()

@end

@implementation BISNavigationTable

- (void)viewDidLoad {
    [super viewDidLoad];
    BISTableViewController *table = [[BISTableViewController alloc]initWithStyle:UITableViewStylePlain];
    self.viewControllers = @[table];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
