//
//  BISFlickrCell.m
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISFilterOperation.h"
#import "BISFlickrCell.h"
#import "BISFlickr.h"
#import <UIKit/UIKit.h>
#import "Masonry.h"

NSString *const BISFlickrCellIdentifier = @"BISFlickrCellIdentifier";

@interface BISFlickrCell()

@property(nonatomic, strong) UIImageView *viewImage;
@property(nonatomic, strong) UILabel *progressLabel;
@property(nonatomic, strong) UIProgressView *progressView;
@property(nonatomic, strong) UIButton *button;


//не знаю как задать таргет кнопке с неск параметрами
//->эти проперти для врем хранения для работы кнопки
@property(nonatomic, copy) NSOperationQueue* filteringQueue;
@property(nonatomic, copy) BISFlickr* flickr;
@property(nonatomic, copy) void(^returnFilteredImage)(BISFlickr*, UIImage*);
@end


@implementation BISFlickrCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createElements];
    }
    return self;
}

-(void)createElements{
    self.viewImage = [UIImageView new];
    [self addSubview:_viewImage];
    [_viewImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(200.f, 200.f));
    }];
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setTitle:@"Filter" forState:UIControlStateNormal];
    _button.backgroundColor = [UIColor blueColor];
    [self addSubview:_button];
    [_button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_viewImage.mas_rightMargin).with.offset(80);
        make.centerY.equalTo(_viewImage.mas_centerY);
    }];
    
    
    self.progressView = [UIProgressView new];
    [self addSubview:_progressView];
    [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(200.f, 200.f));
    }];
    
    self.progressLabel = [UILabel new];
    [self addSubview:_progressLabel];
    [_progressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_progressView);
        make.centerY.equalTo(_progressView).with.offset(-40);
    }];
}


- (UIImage*)getImage{
    return _viewImage.image;
}

/**
 Делаем видимой картинку и помещаем во вью
 */
- (void)addImage:(UIImage*)image{
    _viewImage.hidden = NO;
    _button.hidden = YES;
    _progressLabel.hidden = YES;
    _progressView.hidden = YES;
    _viewImage.image = image;
}

/**
 Делаем видимой кнопку и помещаем во вью
 
 передаются данные для работы кнопки - наложение фильтра
 (в ячейке не должны храниться данные, но пока так)
 */
- (void)addButton:(NSOperationQueue*)filteringQueue flickr:(BISFlickr*)flickr returnFilteredImage: (void(^)(BISFlickr*, UIImage*))returnFilteredImage{
    _filteringQueue = filteringQueue;
    _flickr = flickr;
    _returnFilteredImage = returnFilteredImage;
    
    _viewImage.hidden = NO;
    _button.hidden = NO;
    _progressLabel.hidden = YES;
    _progressView.hidden = YES;
    [_button addTarget:self
                action:@selector(applyFilterToImage:)
     forControlEvents:UIControlEventTouchUpInside];
}

/**
  Таргет для кнопки, запускает операцию наложения фильтра
 */
- (void)applyFilterToImage:(id) sender{
    BISFilterOperation *filterOperation = [[BISFilterOperation alloc]initWithFlickr:_flickr imageToFilter:_viewImage.image returnFilteredImage:_returnFilteredImage];
    [_filteringQueue addOperation:filterOperation];
}

/**
  Делаем видимым прогресс бар картинки, процент и объеми скачивания 
  ,присваиваем данные
 */
- (void)addProgressInfo:(BISFlickr*)flickr{
    _viewImage.hidden = YES;
    _button.hidden = YES;
    _progressLabel.hidden = NO;
    _progressView.hidden = NO;
    _progressView.progress = flickr.progressFloat;
    _progressLabel.text = [NSString stringWithFormat:@"%.1f%% of %@",flickr.progressFloat*100, flickr.totalSize];
}

+ (CGFloat)heightForCell{
    return 220;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
