//
//  BISFlickrOperation.h
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BISFlickr.h"

@interface BISFlickrOperation : NSOperation

@property(nonatomic, strong) NSURLSession *session;
@property(nonatomic, strong) BISFlickr *flickr;
@property(nonatomic, strong) void(^returnProgressOrImage)(BISFlickr*, UIImage*);

-(instancetype)initWithSession: (NSString *) identifier;

@end
