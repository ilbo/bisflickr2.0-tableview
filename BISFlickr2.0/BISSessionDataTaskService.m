//
//  BISSessionDataTaskService.m
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISSessionDataTaskService.h"
#import <WebKit/WebKit.h>

static BISSessionDataTaskService *sharedTaskService;

@implementation BISSessionDataTaskService

/**
 Запускает таск и запускает обработку общих данный

 @param url обработанный url для запроса
 @param returnArray блок, передающий данные в table view
 */
- (void)getArrayOfUrlDictionaries:(NSURL*)url returnInBlock: (void(^)(NSArray *urlDictionaries))returnArray{
    self.returnArray = returnArray;
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"ERROR with url: %@ \n %@", url, error.localizedDescription);
        }
        else {
            NSHTTPURLResponse *resp =(NSHTTPURLResponse *)response;
            if (resp.statusCode==200){
                [self processingData:data compliteBlock:_returnArray];
            }
        }
    }];
    [task resume];
}

/**
 Обработка общих данных
 
 @param data ...
 @param returnArray блок переносит массив словарей фотографий в table view
 */
- (void)processingData:(NSData*)data compliteBlock:(void(^)(NSArray *urlDictionaries))returnArray{
    NSError *error;
    NSDictionary *dictionaryObj = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    NSString *errorString = [dictionaryObj valueForKey:@"error"];
    if(errorString){
        NSLog(@"PROCESSING ERROR!: %@", errorString);
    }
    if(!error){
        NSArray *commonDictionaries = [dictionaryObj valueForKey:@"photos"];
        NSArray *urlDictionaries = [commonDictionaries valueForKey:@"photo"];
        _returnArray(urlDictionaries);
    }
}

@end
