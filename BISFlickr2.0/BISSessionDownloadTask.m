//
//  BISSessionDownloadTask.m
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISSessionDownloadTask.h"

@interface BISSessionDownloadTask()<NSURLSessionDownloadDelegate>

@property(nonatomic, strong) NSURLSession *session;

@end

@implementation BISSessionDownloadTask

-(NSURLSession *) getSessionWithIdentifier: (NSString *) identifier{
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
    sessionConfig.HTTPMaximumConnectionsPerHost = 40;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    return session;
}

-(void)requestWithUrl:(NSURL *)url flickrObject: (BISFlickr *) flickr itemToOperationBlock: (void(^)(BISFlickr *))itemToOperationBlock{
    self.itemToOperationBlock = itemToOperationBlock;
    self.flickr = flickr;
    NSLog(@"image url = %@", [url absoluteString]);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDownloadTask*task = [_session downloadTaskWithURL: url];
    [task resume];
    //_itemToOperationBlock(flickr);
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location{
    NSLog(@"загрузка завершена, location = %@", [location absoluteString]);
    void (^blockName)(NSURL * , BISFlickr *);
    blockName = ^void(NSURL *location, BISFlickr * flickr) {
        NSData *imageData = [NSData dataWithContentsOfURL:location];
        flickr.image = [UIImage imageWithData:imageData];
        _itemToOperationBlock(flickr);
    };
    dispatch_async(dispatch_get_main_queue(),^{
        blockName(location , _flickr);
    });
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if(error != nil){
        NSLog(@"загрузка завершена c error : %@", error);
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes{
    
}

/* Sent periodically to notify the delegate of download progress. */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    NSLog(@"_____%.1lld%%", totalBytesWritten/totalBytesExpectedToWrite*100);
    
    void (^blockName)(BISFlickr *);
    blockName = ^void(BISFlickr * flickr) {
        CGFloat progressFloat = totalBytesWritten / totalBytesExpectedToWrite;
        flickr.progressFloat = progressFloat;
        NSString *totalSize = [NSByteCountFormatter stringFromByteCount:totalBytesExpectedToWrite countStyle:NSByteCountFormatterCountStyleBinary];
        flickr.progressString = [NSString stringWithFormat:@"%.1f%% of %@",progressFloat*100, totalSize];
        _itemToOperationBlock(flickr);
    };
    dispatch_async(dispatch_get_main_queue(),^{
        blockName(_flickr);
    });
}


@end
