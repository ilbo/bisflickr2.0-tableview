//
//  main.m
//  BISFlickr2.0
//
//  Created by iliya on 25.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
