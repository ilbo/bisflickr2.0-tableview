//
//  BISSettingsRequest.m
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISSettingsRequest.h"

@implementation BISSettingsRequest

- (instancetype)initWithSession:(NSString*)identifier{
    self = [super init];
    if(self){
        self.session = [self getSessionWithIdentifier:identifier];
    }
    return self;
}

/**
 Собирает url из строки

 @param searchText ...
 @return ...
 */
+ (NSURL*)getPhotosUrl:(NSString*)searchText{
    
    NSMutableString *url = [NSMutableString new];
    [url appendString:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&text="];
    [url appendString: [BISSettingsRequest getOptimizedSearchText:searchText]];
    [url appendString:@"&api_key=c55f5a419863413f77af53764f86bd66&format=json&nojsoncallback=1"];
    return [NSURL URLWithString:url];
}

/**
  Вместо пробелов ставит "+"
 */
+ (NSString *)getOptimizedSearchText:(NSString *)searchText{
    NSString *optSearchText = @"";
    if(searchText){
        NSArray *optSearchArray = [searchText componentsSeparatedByString:@" "];
        optSearchText = [optSearchArray componentsJoinedByString:@"+"];
    }else{
        NSLog(@"SEARCH TEXT IS EMPTY!");
    }
    return optSearchText;
}

- (NSURLSession *)getSessionWithIdentifier:(NSString *)identifier{
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
    sessionConfig.HTTPMaximumConnectionsPerHost = 40;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    return session;
}

@end
