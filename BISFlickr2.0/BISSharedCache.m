//
//  BISSharedCache.m
//  BISFlickr2.0
//
//  Created by iOS-School-1 on 27.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISSharedCache.h"
static BISSharedCache *sharedInstance;

@interface BISSharedCache ()
@property (nonatomic, strong) NSCache *imageCache;
@end

@implementation BISSharedCache

+ (BISSharedCache*)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BISSharedCache alloc] init];
    });
    return sharedInstance;
}
- (instancetype)init {
    self = [super init];
    if (self) {
        self.imageCache = [[NSCache alloc] init];
    }
    return self;
}

- (void)cacheImage:(UIImage*)image forKey:(NSURL*)key {
    [self.imageCache setObject:image forKey:[key absoluteString]];
}

- (UIImage*)getCachedImageForKey:(NSURL*)key {
    return [self.imageCache objectForKey:[key absoluteString]];
}

@end
