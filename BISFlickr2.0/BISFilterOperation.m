//
//  BISFilterOperation.m
//  BISFlickr2.0
//
//  Created by iliya on 27.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISFilterOperation.h"

@interface BISFilterOperation()

{
    BOOL executing;
    BOOL finished;
}

@end

@implementation BISFilterOperation

- (instancetype)initWithFlickr:(BISFlickr*)flickr imageToFilter:(UIImage*)image returnFilteredImage: (void(^)(BISFlickr*, UIImage*))returnFilteredImage{
    self = [super init];
    if(self){
        self.flickr = flickr;
        self.image = image;
        self.returnFilteredImage = returnFilteredImage;
    }
    return self;
}

- (void)main{
    @try {
        @autoreleasepool {
            
            //наложение фильтра
            CIImage *outImg = [[CIImage alloc] initWithImage:_image];
            CIFilter *invertColor = [CIFilter filterWithName:@"CIColorControls"];
            [invertColor setValue:outImg forKey:@"inputImage"];
            [invertColor setValue:@10.0 forKey:@"inputSaturation"];
            CIImage* outImgFilter = [invertColor outputImage];
            UIImage* endImg = [UIImage imageWithCIImage:outImgFilter];
            
            
            _returnFilteredImage(_flickr, endImg);
            
            // Соответствие KVO. Генерируем требуемые уведомления KVO.
            [self willChangeValueForKey:@"isFinished"];
            [self willChangeValueForKey:@"isExecuting"];
            finished = YES;
            executing = NO;
            [self didChangeValueForKey:@"isFinished"];
            [self didChangeValueForKey:@"isExecuting"];
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception in Operation %@", e);
    }
}

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing;
}

- (BOOL)isFinished {
    return finished;
}


@end
