//
//  BISSettingsRequest.h
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BISSettingsRequest : NSObject<NSURLSessionDelegate>

@property(nonatomic, strong) NSURLSession *session;

- (instancetype)initWithSession:(NSString*)identifier;
+ (NSURL*)getPhotosUrl:(NSString*)searchText;

@end
