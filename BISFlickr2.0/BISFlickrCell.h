//
//  BISFlickrCell.h
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BISFlickr;

/*! идентификатор ячейки */
extern NSString *const BISFlickrCellIdentifier;

@interface BISFlickrCell : UITableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier;
- (UIImage*)getImage;
- (void)addImage:(UIImage*)image;
- (void)addProgressInfo:(BISFlickr*)flickr;
- (void)addButton:(NSOperationQueue*)filteringQueue flickr:(BISFlickr*)flickr returnFilteredImage: (void(^)(BISFlickr*, UIImage*))returnFilteredImage;
+ (CGFloat)heightForCell;

@end
