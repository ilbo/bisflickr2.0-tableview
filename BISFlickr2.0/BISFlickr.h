//
//  BISFlickr.h
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BISFlickr : NSObject


@property (nonatomic, strong) NSURL *urlImage;
@property (nonatomic) float progressFloat;
@property (nonatomic) NSString *totalSize;
@property (nonatomic) BOOL isImageDownloaded;

- (instancetype)initWithDictionary:(NSDictionary*)dict;

@end
