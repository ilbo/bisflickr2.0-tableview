//
//  BISTableViewController.m
//  BISFlickr2.0
//
//  Created by iliya on 25.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISTableViewController.h"
#import "BISSessionDataTaskService.h"
#import "BISFilterOperation.h"
#import "BISFlickrOperation.h"
#import "BISSettingsRequest.h"
#import "BISSharedCache.h"
#import "BISFlickrCell.h"
#import "BISFlickr.h"

@interface BISTableViewController ()<UISearchBarDelegate>

@property(nonatomic, strong) BISSessionDataTaskService *dataTaskService;
@property(nonatomic, strong) BISSettingsRequest *settings;
@property(nonatomic, strong) UISearchBar *searchBar;

/*! промежуточное хранилище для возвращаемой общей инфы */
@property(nonatomic, strong) NSArray *urlDictionaries;

/*! массив объектов с url, прогрессом, флагом-загружена ли фотка */
@property(nonatomic, strong) NSMutableArray<BISFlickr*> *flickrs;

@property(nonatomic, strong) NSOperationQueue *queue;

@property(nonatomic, strong) BISSharedCache *cache;

/*! словарь из запущенных операций скачивания с ключом indexPath */
@property(nonatomic, strong) NSMutableDictionary<NSIndexPath*,BISFlickrOperation*> *downloadingFlickr;

/*! словарь из запущенных операций фильтрации с ключом indexPath */
@property(nonatomic, strong) NSOperationQueue *filteringQueue;


@end

@implementation BISTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createAndRegistrationCell];
    [self addSearchBar];
    [self createTaskService];
    [self setSettings];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return _flickrs.count;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath{
    return [BISFlickrCell heightForCell];
}

/**
 Метод перед отрисовкой каждой ячейки
 
 *1)Если не существует операции для данной ячейки + еще не загружена даннаяя фотка - создаем операцию
 блок возвращает:
 или прогресс, тогда присваиваем прогресс ячейке
 или фотку, тогда добавляем в кэш и присваиваем ячейке
 
 *2)Если не работает операция для скачивания и фотка загружена - берем ее из кэша
 
 *При медленном интернете заметно как видимые ячейки переиспользуются другими операциями
 
 *3)Рядом с фоткой появляется кнопка для наложения фильтра
 
 */
- (void)tableView:(UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_downloadingFlickr.count == 0){
     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    BISFlickrOperation *haveOperation = _downloadingFlickr[indexPath];
    
    //(1)
    if(!haveOperation
       && _flickrs[indexPath.row].isImageDownloaded == NO){
        
        BISFlickrOperation *operation = operation = [[BISFlickrOperation alloc] initWithSession: [_flickrs[indexPath.row].urlImage absoluteString]];
        [operation setFlickr:_flickrs[indexPath.row]];
        [operation setReturnProgressOrImage:^(BISFlickr* flickr, UIImage* image){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _flickrs[indexPath.row] = flickr;
                
                if(!image){
                    //работает прогресс бар
                    [(BISFlickrCell*)cell addProgressInfo: flickr];
                }else{
                    //ищем картинку в кэше
                    _cache = [BISSharedCache sharedInstance];
                    if(![_cache getCachedImageForKey:flickr.urlImage]){
                        //не находим и добавляем
                        _flickrs[indexPath.row].isImageDownloaded = flickr.isImageDownloaded;
                        [_cache cacheImage:image forKey:flickr.urlImage];
                        [(BISFlickrCell*)cell addImage:image];
                    }
                    [_downloadingFlickr removeObjectForKey:indexPath];
                    
                    //(3)
                    [self addButton:cell indexPath:indexPath];
                }
            });
        }];
        //добаляем операцию для учета и в очередь, она теперь работает
        _downloadingFlickr[indexPath] = operation;
        [_queue addOperation:operation];
        
    }else{
        //(2)
        UIImage *foundImageInCache = [_cache getCachedImageForKey:_flickrs[indexPath.row].urlImage];
        [(BISFlickrCell*)cell addImage:foundImageInCache];
        
        //(3)
        [self addButton:cell indexPath:indexPath];
    }
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    UITableViewCell *reuseCell = (BISFlickrCell*)[tableView dequeueReusableCellWithIdentifier:BISFlickrCellIdentifier forIndexPath:indexPath];
    if(!reuseCell){
        reuseCell = [[BISFlickrCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: BISFlickrCellIdentifier];
    }
    return reuseCell;
}


/**
 Добавляем поиск
 */
- (void)addSearchBar{
    if(!_searchBar){
        _searchBar=[[UISearchBar alloc]init];
    }
    _searchBar.placeholder = @"Please, insert the name of the image";
    [_searchBar sizeToFit];
    self.navigationItem.titleView = _searchBar;
    _searchBar.delegate=self;
    [_searchBar becomeFirstResponder];
}

/**
 Создаем ячейку единожды для переиспользования и регистрируем ее идентификатор
 */
- (void)createAndRegistrationCell{
    [self.tableView registerClass:[BISFlickrCell class] forCellReuseIdentifier:BISFlickrCellIdentifier];
}

/**
 Обработка нажатия Enter на поисковом элементе
 
 Запускаем таск для общих данных c блоком для получения словаря из таска
 
 Создается массив объектов из словаря с флагом - не скачано
 */
- (void)searchBarSearchButtonClicked:(UISearchBar*)searchBar{
    [searchBar endEditing:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if(_queue){
        [_queue cancelAllOperations];
    }
    self.queue = [NSOperationQueue new];
    self.downloadingFlickr = [NSMutableDictionary new];
    self.filteringQueue = [NSOperationQueue new];
    self.flickrs = [NSMutableArray new];
    [self.tableView reloadData];
    
    NSURL *url = [BISSettingsRequest getPhotosUrl:searchBar.text];
    [_dataTaskService getArrayOfUrlDictionaries: url returnInBlock:^(NSArray* urlDictionaries){
        _urlDictionaries = urlDictionaries;
        for (NSUInteger i=0; i<_urlDictionaries.count; i++) {
            BISFlickr *flickr = [[BISFlickr alloc]initWithDictionary:_urlDictionaries[i]];
            _flickrs[i] = flickr;
        }
        [self.tableView reloadData];
    }];
    
}

- (void)addButton:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    [(BISFlickrCell*)cell addButton:_filteringQueue flickr:_flickrs[indexPath.row] returnFilteredImage:^(BISFlickr* flickr, UIImage* image){
        dispatch_async(dispatch_get_main_queue(), ^{
            [_cache cacheImage:image forKey:flickr.urlImage];
            [(BISFlickrCell*)cell addImage:image];
        });
    }];
}

/**
 Создаем сервис для загрузки общих данных
 */
- (void)createTaskService{
    self.dataTaskService = [BISSessionDataTaskService new];
}

/**
  Здесь могут задаваться настройки для запросов
 */
- (void)setSettings{
    //self.settings = [[BISSettingsRequest alloc] initWithSession:@"ru.BIS.FlickrApp"];
}

@end
