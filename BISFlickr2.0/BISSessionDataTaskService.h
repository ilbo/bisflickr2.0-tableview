//
//  BISSessionDataTaskService.h
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BISSessionDataTaskService : NSObject

@property(nonatomic, strong) void (^returnArray)(NSArray *urlDictionaries);

- (void)getArrayOfUrlDictionaries:(NSURL*)url returnInBlock:(void(^)(NSArray *urlDictionaries))returnArray;

@end
