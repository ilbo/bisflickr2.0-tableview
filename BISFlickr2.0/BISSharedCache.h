//
//  BISSharedCache.h
//  BISFlickr2.0
//
//  Created by iOS-School-1 on 27.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BISSharedCache : NSObject

+ (BISSharedCache*)sharedInstance;

- (void)cacheImage:(UIImage*)image forKey:(NSURL*)key;
- (UIImage*)getCachedImageForKey:(NSURL*)key;

@end
