//
//  BISFlickrOperation.m
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISFlickrOperation.h"
#import <UIKit/UIKit.h>

@interface BISFlickrOperation()<NSURLSessionDownloadDelegate>
{
    BOOL executing;
    BOOL finished;
}

@end

@implementation BISFlickrOperation

-(instancetype)initWithSession: (NSString *) identifier{
    self = [super init];
    if(self){
        self.session = [self getSessionWithIdentifier:identifier];
    }
    return self;
}

-(NSURLSession *) getSessionWithIdentifier: (NSString *) identifier{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.HTTPMaximumConnectionsPerHost = 40;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    return session;
}

- (void)main{
    @try {
        @autoreleasepool {
            
            [self runTaskWithUrl:_flickr.urlImage];
            
            // Соответствие KVO. Генерируем требуемые уведомления KVO.
            [self willChangeValueForKey:@"isFinished"];
            [self willChangeValueForKey:@"isExecuting"];
            finished = YES;
            executing = NO;
            [self didChangeValueForKey:@"isFinished"];
            [self didChangeValueForKey:@"isExecuting"];
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception in Operation %@", e);
    }
}

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing;
}

- (BOOL)isFinished {
    return finished;
}

- (void)runTaskWithUrl:(NSURL*)url {
    //NSLog(@"image url = %@", [url absoluteString]);
    NSURLSessionDownloadTask*task = [_session downloadTaskWithURL: url];
    [task resume];
}


- (void)URLSession:(NSURLSession*)session downloadTask:(NSURLSessionDownloadTask*)downloadTask
didFinishDownloadingToURL:(NSURL *)location{
    //NSLog(@"загрузка завершена, location = %@", [location absoluteString]);
    NSData *imageData = [NSData dataWithContentsOfURL:location];
    UIImage *image = [UIImage imageWithData:imageData];
    _flickr.isImageDownloaded = YES;
    finished = YES;
    _returnProgressOrImage(_flickr, image);
}

- (void)URLSession:(NSURLSession*)session task:(NSURLSessionTask*)task didCompleteWithError:(NSError *)error{
    if(error){
        NSLog(@"загрузка завершена c error : %@", error);
    }
}

- (void)URLSession:(NSURLSession*)session downloadTask:(NSURLSessionDownloadTask*)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes{
    
}

- (void)URLSession:(NSURLSession*)session downloadTask:(NSURLSessionDownloadTask*)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    //NSLog(@"_____%.1lld%%", totalBytesWritten/totalBytesExpectedToWrite*100);
    
    float progressFloat = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    
    NSString *totalSize = [NSByteCountFormatter stringFromByteCount:totalBytesExpectedToWrite countStyle:NSByteCountFormatterCountStyleBinary];
    _flickr.progressFloat = progressFloat;
    _flickr.totalSize = totalSize;
    _returnProgressOrImage(_flickr, nil);

}


@end
