//
//  BISFlickr.m
//  BISFlickr2.0
//
//  Created by iliya on 26.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISFlickr.h"

/**
 Создает объект по индексу из словаря с данными 
 */
@implementation BISFlickr : NSObject

- (instancetype)initWithDictionary:(NSDictionary*)dict{
    NSString *serverId = dict[@"server"];
    NSString *farmId = [NSString stringWithFormat:@"%@", dict[@"farm"]];
    NSString *imageId = dict[@"id"];
    NSString *secret = dict[@"secret"];
    
    BISFlickr* (^createFlickr)(NSString*, NSString*, NSString*, NSString*);
    createFlickr = ^BISFlickr*(NSString *serverId,
                               NSString *farmId,
                               NSString *imageId,
                               NSString *secret) {
        BISFlickr *flickr = [BISFlickr new];
        flickr.urlImage = [self getUrlString:farmId serverId:serverId imageId:imageId secret:secret];
        flickr.progressFloat = 0.0f;
        flickr.isImageDownloaded = NO;
        return flickr;
    };
    return createFlickr(serverId,farmId,imageId,secret);
}

- (NSURL*)getUrlString:(NSString*)farmId serverId:(NSString*)serverId imageId: (NSString*)imageId secret:(NSString*)secret{
    NSMutableString *url = [NSMutableString new];
    [url appendString:@"https://farm"];
    [url appendString: farmId];
    [url appendString:@".staticflickr.com/"];
    [url appendString: serverId];
    [url appendString: @"/"];
    [url appendString: imageId];
    [url appendString: @"_"];
    [url appendString: secret];
    [url appendString: @"_b.jpg"];
    return [NSURL URLWithString:url];
}


@end
